# Telluriq

Telluriq is an experimental *graphical user interface* that monitors and calibrates haptic floors.
It provides an easy way to manipulate a range of [D-Box](https://www.d-box.com) actuators in order to prepare experimental performances with the haptic technology.

![Screenshots](assets/png/screenshots.png)

## The haptic floor

The haptic floor is a floor that provides sensation of movements. Telluriq has been prototyped for a floor with 10 triangular frames.

![Floor 2D](assets/png/prototype2D.png)
