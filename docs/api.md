# OSC API

We provide an OSC API in order to make a communication between the Telluriq GUI and the haptic floor.

???+ check "`/signal value`"

    Accepts OSC messages that contains a single [IEEE 754 32-bit base-2 floating-point value](https://en.wikipedia.org/wiki/Single-precision_floating-point_format) in the range -1.0 to 1.0.
    Any valid floating-point value received on this endpoint is transferred to the actuators (clockwise).
