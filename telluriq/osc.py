import pyOSC3
import socket
import logging
import select

logger = logging.getLogger("console")


class OSCMultiClient(pyOSC3.OSCMultiClient):
    """subclass of `pyOSC3.OSCMultiClient` to fix invalid syntax

    some parts of pyOSC3 are buggy or simply not badly written so
    we need to do the dirty work

    Extends:
        pyOSC3.OSCMultiClient
    """

    def setOSCTarget(self, address, prefix=None, filters=None):
        """Add (i.e. subscribe) a new OSCTarget, or change the prefix for an existing OSCTarget.
        the 'address' argument can be a ((host, port) tuple) : The target server address & UDP-port
          or a 'host' (string) : The host will be looked-up
        - prefix (string): The OSC-address prefix prepended to the address of each OSCMessage
        sent to this OSCTarget (optional)
        """
        if type(address) is str:
            address = self._searchHostAddr(address)

        elif type(address) == tuple:
            (host, port) = address[:2]
            try:
                host = socket.gethostbyname(host)
            except:
                pass

            address = (host, port)
        else:
            raise TypeError(
                "'address' argument must be a (host, port) tuple or a 'host' string"
            )

        self._setTarget(address, prefix, filters)

    def delOSCTarget(self, address, prefix=None):
        """Delete the specified OSCTarget from the Client's dict.
        the 'address' argument can be a ((host, port) tuple), or a hostname.
        If the 'prefix' argument is given, the Target is only deleted if the address and prefix match.
        """
        if type(address) is str:
            address = self._searchHostAddr(address)

        if type(address) == tuple:
            (host, port) = address[:2]
            try:
                host = socket.gethostbyname(host)
            except socket.error:
                pass
            address = (host, port)

            self._delTarget(address, prefix)

    def send(self, path, *data, timeout=None, to=None):
        # check for socket file descriptor attribute
        if not hasattr(self, "_fd"):
            # @NOTE: IPv4 is enforced here since discovered addresses
            #        from the zeroconf service listener are parsed
            #        using `IPVersion.V4Only` as a version parameter to
            #        the method `parsed_addresses` of a service `info` object
            self._setSocket(socket.socket(socket.AF_INET, socket.SOCK_DGRAM))

        try:
            if isinstance(path, pyOSC3.OSCBundle) or isinstance(
                path, pyOSC3.OSCMessage
            ):
                if to is None:
                    # send OSCMessage or OSCBundle to all targets
                    super().send(path, timeout)
                else:
                    # send OSCMessage or OSCBundle to specific target
                    self.sendto(path, to, timeout)
            else:
                if to is None:
                    # compute OSCMessage and send to all targets
                    super().send(pyOSC3.OSCMessage(path, *data), timeout)
                else:
                    # compute OSCMessage and send to specific target
                    self.sendto(pyOSC3.OSCMessage(path, *data), to, timeout)
        except pyOSC3.OSCClientError as e:
            logger.error(str(e))

    def sendto(self, msg, address, timeout=None):
        if not isinstance(msg, pyOSC3.OSCMessage):
            raise TypeError("'msg' argument is not an OSCMessage or OSCBundle object")

        ret = select.select([], [self._fd], [], timeout)
        try:
            ret[1].index(self._fd)
        except:
            # for the very rare case this might happen
            raise pyOSC3.OSCClientError("Timed out waiting for file descriptor")

        binary = msg.getBinary()

        try:
            while len(binary):
                self._ensureConnected(address)
                sent = self.socket.sendto(binary, address)
                binary = binary[sent:]
        except ConnectionRefusedError:
            pass
        except socket.error as e:
            if e[0] in (
                7,  # 7 = 'no address associated with nodename'
                65,  # 65 = 'no route to host'
            ):
                raise e
            else:
                raise pyOSC3.OSCClientError(
                    "while sending to %s: %s" % (str(address), str(e))
                )
