from kivy.uix.button import Button
from kivy.uix.widget import Widget
from kivy.properties import StringProperty
from kivy.app import App
import pyOSC3
import logging
import random

logger = logging.getLogger("console")


class HelpButton(Button):
    def on_press(self):
        logger.info("help")


class CalibrateButton(Button):
    def _get_random_bundle(self):
        bundle = pyOSC3.OSCBundle("/signal")
        for _ in range(25):
            bundle.append(
                pyOSC3.OSCMessage("/signal", [random.random(), 0.0, 0.0, 0.0])
            )
        for _ in range(25):
            bundle.append(pyOSC3.OSCMessage("/signal", [0.0, 0.0, 0.0, 0.0]))
        for _ in range(25):
            bundle.append(
                pyOSC3.OSCMessage("/signal", [0.0, random.random(), 0.0, 0.0])
            )
        for _ in range(25):
            bundle.append(pyOSC3.OSCMessage("/signal", [0.0, 0.0, 0.0, 0.0]))
        for _ in range(25):
            bundle.append(
                pyOSC3.OSCMessage("/signal", [0.0, 0.0, random.random(), 0.0])
            )
        for _ in range(25):
            bundle.append(pyOSC3.OSCMessage("/signal", [0.0, 0.0, 0.0, 0.0]))
        for _ in range(25):
            bundle.append(
                pyOSC3.OSCMessage("/signal", [0.0, 0.0, 0.0, random.random()])
            )
        for _ in range(25):
            bundle.append(pyOSC3.OSCMessage("/signal", [0.0, 0.0, 0.0, 0.0]))
        return bundle

    def on_press(self):
        if not hasattr(self, "app"):
            self.app = App.get_running_app()
        logger.info("calibrate")
        try:
            bundle = self._get_random_bundle()
            self.app.osc.send(bundle)

        except pyOSC3.OSC3.OSCClientError as e:
            logger.error(str(e))


class SettingsButton(Button):
    def on_press(self):
        logger.info("settings")


class LogFilterButton(Button):
    level = "INFO"

    def on_press(self):
        pass
        # handler = logger.handlers[0]
        # console = handler.root
        # label = console.children[0]
