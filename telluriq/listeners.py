from zeroconf import IPVersion
import logging

# Button:
#   size_hint_x: None
#   text: "Activation"
#   on_press:
#     app.osc.sendMessage("/logs/activate", 9001)
# Button:
#   size_hint_x: None
#   text: "Désactivation"
#   on_press:
#     app.osc.sendMessage("/logs/deactivate", [])


class KivyOSCServiceListener:

    logger = logging.getLogger("console")

    def __init__(self, client, datatable):
        self.osc = client
        self.datatable = datatable
        self.discovered_infos = {}

    def remove_service(self, zeroconf, type_, name):
        info = self.discovered_infos[name]
        address = info.parsed_addresses(version=IPVersion.V4Only)[0], info.port
        self.logger.info(
            f"zeroconf: service `{name.split('_')[0]}` from `{info.server.split('.')[0]}` removed"
        )
        self.osc.delOSCTarget(address)
        self.osc.send("/logs/deactivate", to=address)

        idx = self.datatable.row_data.index(
            (
                name.split("_")[0],
                info.server,
                address[0],
                str(address[1]),
                name.split(".")[0].split("_")[1],
                type_,
            )
        )
        self.datatable.row_data.pop(idx)
        del self.discovered_infos[name]

    def add_service(self, zeroconf, type_, name):
        info = zeroconf.get_service_info(type_, name)
        if info:
            self.discovered_infos[name] = info
            address = info.parsed_addresses(version=IPVersion.V4Only)[0], info.port
            self.logger.info(
                f"zeroconf: service `{name.split('_')[0]}` from `{info.server.split('.')[0]}` added, IP address: {address[0]}:{address[1]}"
            )
            self.osc.setOSCTarget(address)
            self.osc.send("/logs/activate", 9001, to=address)

            # for row in self.datatable.row_data:
            # if row[1] == info.server:

            self.datatable.row_data.append(
                (
                    name.split("_")[0],
                    info.server,
                    address[0],
                    str(address[1]),
                    name.split(".")[0].split("_")[1],
                    type_,
                )
            )

    def update_service(self, zeroconf, type_, name):
        prev_info = self.discovered_infos[name]
        info = zeroconf.get_service_info(type_, name)
        if info:
            prev_address = (
                prev_info.parsed_addresses(version=IPVersion.V4Only)[0],
                prev_info.port,
            )
            address = info.parsed_addresses(version=IPVersion.V4Only)[0], info.port

            if prev_address != address:
                self.osc.delOSCTarget(prev_address)
                self.osc.setOSCTarget(address)

            self.osc.send("/logs/activate", 9001, to=address)

            # find idx for service row
            idx = None
            for row in self.datatable.row_data:
                if row[0] == name.split("_")[0] and row[-1] == type_:
                    idx = self.datatable.row_data.index(row)

            # update datatable row
            if idx:
                self.datatable.row_data[idx] = (
                    name.split("_")[0],
                    info.server,
                    address[0],
                    str(address[1]),
                    name.split(".")[0].split("_")[1],
                    type_,
                )

            # update discovered infos
            self.discovered_infos[name] = info
