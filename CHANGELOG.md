Release Notes
===================

Telluriq 0.2.0 (2022-04-27)
----------------------------------

* ✨ Stable logs streaming, OSC support and UI improvements
* ✨ Setup inter font as default font for current app
* 🧑‍💻 Update the issue templates
* 🐛 Fix journal viewer with too many logs
* 📝 Update current osc api documentation
* 📝 Fix quick start documentation and improve makefile
* 📝 Update README with documentation commands
* 📝 Setup a documentation site with mkdocs

Telluriq 0.1.0 (2022-02-01)
----------------------------------

New version of Telluriq rewritten using [Kivy](https://kivy.org/) to provide better multi-platform support.

* ✨ Add support for multipages
* 🧑‍💻 Add Feature Request and Bug Resolution Request templates
* 👷 Setup a CI for Kivy
* 💄 Add log filters
* 💄 Display colored log messages
* 📄 Add GPLv3 license

Telluriq 0.0.1 (2021-12-03)
---------------------------------

Prototype version of Telluriq made with [tkinter](https://docs.python.org/3/library/tkinter.html#module-tkinter) and [Tkinter-Designer](https://github.com/ParthJadhav/Tkinter-Designer).

- 🚨 Reformat code with black linter
- 🐛 fix typing import / types
- ✨ Integrate log message decorations
- 💄 Implémenter feuille css champs de texte
- 👷 Add a CI for tkinter
- 🎨 Transpose auto-generated code from tkdesigner to components
- 📝 Add a minimal README in order to install and work with this project\n
